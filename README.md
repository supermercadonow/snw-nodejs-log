# Supermercado Now - Log

Pacote de logs para NodeJS, para a utilização do nosso servidor de log.

## Instalação

Primeiro será necessário adicionar o repositório privado do pacote

```
npm install --save git+https://bitbucket.org/supermercadonow/snw-nodejs-log.git
```

ou

```
yarn add git+https://bitbucket.org/supermercadonow/snw-nodejs-log.git
```

## Utilização

Será necessário definir algumas variáveis no `.env`:

```
APP_ENV=development
APP_NAME=Application
GRAYLOG_HOST=54.232.98.146
GRAYLOG_PORT=12202
```

## Funcionamento

Para utilizar o log através do pacote, será necessario importar o método `logger` e enviar os dados desejados 

Por exemplo:
```
const { logger } = require('snw-nodejs-log)
logger.info("Some message", {"foo": "bar", "bar": {"id": 1, "name" : "Teste"}});
```
