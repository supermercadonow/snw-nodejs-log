'use strict';

const ip = require('ip');
const winston = require('winston');

const winstonGraylog2 = require('./graylog');

const {
  APP_ENV,
  APP_NAME,
  GRAYLOG_HOST,
  GRAYLOG_PORT
} = process.env;

const options = {
    name: 'Graylog',
    silent: false,
    handleExceptions: false,
    graylog: {
        servers: [{
            host: GRAYLOG_HOST, 
            port: GRAYLOG_PORT || 12202
        }],
        bufferSize: 1400
    },
    staticMeta: {
        application: APP_NAME || '',
        env: APP_ENV || 'development',
        source_ip: ip.address()
    }
};

function isProduction () {
  return !!APP_ENV && APP_ENV.toLowerCase() === 'production'
}

const logFormat = winston.format.printf(function(info) {
  const { message, level, ...rest } = info
  
  return `${level}: ${JSON.stringify(message, null, 4)} - ${JSON.stringify(rest)}`;
});

const productionTransports = [
  new winstonGraylog2(options)
];

const developmentTransports = [
  new winston.transports.Console({
    format: winston.format.combine(winston.format.colorize(), logFormat)
  })
];

const logger = winston.createLogger({
  format: winston.format.json(),
  transports: isProduction() ? productionTransports :  developmentTransports,
});

module.exports = { logger }